#!/usr/bin/env python3

import rainbowhat as rh
import time,colorsys

rh.rainbow.set_brightness(0.04)

def setPixel(x):
    h = (x * 0.1) % 360
    r, g, b = [int(c * 255) for c in colorsys.hsv_to_rgb(h, 1.0, 1.0)]
    rh.rainbow.set_pixel(x,r,g,b)

@rh.touch.A.press()
def touch_a(channel):
    ps = str(rh.weather.pressure()).split('.')[0]
    p = rh.weather.pressure()

    if p > 1030:
        setPixel(0)
    if p > 1020:
        setPixel(1)
    if p > 1010:
        setPixel(2)
    if p > 1000:
        setPixel(3)
    if p > 990:
        setPixel(4)
    if p > 980:
        setPixel(5)
    if p > 970:
        setPixel(6)
    rh.rainbow.show()

    rh.display.print_str(ps)
    rh.display.show()
    rh.lights.rgb(1, 0, 0)

@rh.touch.B.press()
def touch_b(channel):
    zeit = time.strftime('%H%M')
    rh.display.print_str(zeit)
    rh.display.show()
    rh.lights.rgb(0, 1, 0)

@rh.touch.B.release()
def release_b(channel):
    rh.display.clear()
    rh.display.show()
    rh.lights.rgb(0, 0, 0)

@rh.touch.A.release()
def release_a(channel):
    rh.rainbow.clear()
    rh.rainbow.show()
    rh.display.clear()
    rh.display.show()
    rh.lights.rgb(0, 0, 0)

while True:
    time.sleep(1)
